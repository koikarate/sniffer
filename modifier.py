from scapy.layers.inet import Ether, IP, UDP, TCP
import scapy.all
from scapy.all import *
import dpkt
import argparse
import configparser
import os


def checksum(packet):
    del packet[1].chksum
    del packet[2].chksum
    packet = packet.__class__(bytes(packet))
    return packet


class Modifier:
    def __init__(self, mod_traf_fame, drop_traf_fname):
        self.mod_traf_fname = mod_traf_fame
        self.drop_traf_fname = drop_traf_fname

        config = configparser.ConfigParser()
        config.read("traffic_modifier_settings.ini")

        self.client = {'ip': config["Spoof"]["client_ip"],
                       'port': int(config["Spoof"]["client_port"]),
                       'mac': config["Spoof"]["client_mac"]}
        self.old_server = {'ip': config["Spoof"]["old_server_ip"],
                           'port': int(config["Spoof"]["old_server_port"]),
                           'mac': config["Spoof"]["old_server_mac"]}
        self.new_server = {'ip': config["Spoof"]["new_server_ip"],
                           'port': int(config["Spoof"]["new_server_port"]),
                           'mac': config["Spoof"]["new_server_mac"]}

        # данные для запрета обмена сообщениями
        self.first_node = {'ip': config["Prohibition"]["first_node_ip"],
                           'port': int(config["Prohibition"]["first_node_port"]),
                           'mac': config["Prohibition"]["first_node_mac"]}
        self.second_node = {'ip': config["Prohibition"]["second_node_ip"],
                            'port': int(config["Prohibition"]["second_node_port"]),
                            'mac': config["Prohibition"]["second_node_mac"]}


    @staticmethod
    def filewriter(filename, packet):
        wrpcap(filename, packet, append=True)

    def modify(self, packet):

        # подмена пакета по пути от клиента А к серверу С на сервер D
        if packet.haslayer('TCP') and packet.haslayer('IP'):
            if (packet['IP'].src == self.client['ip']
                    and packet['TCP'].sport == self.client['port']
                    and packet['IP'].dst == self.old_server['ip']
                    and packet['TCP'].dport == self.old_server['port']):
                packet['Ether'].dst = self.new_server['mac']
                packet['IP'].dst = self.new_server['ip']
                packet['TCP'].dport = self.new_server['port']

                packet = checksum(packet)
                self.filewriter(self.mod_traf_fname, packet)

            # обратная подмена
            elif packet['IP'].src == self.new_server['ip'] \
                    and packet['TCP'].sport == self.new_server['port']\
                    and packet['IP'].dst == self.client['ip']\
                    and packet['TCP'].dport == self.client['port']:
                packet['Ether'].src = self.old_server['mac']
                packet['IP'].src = self.old_server['ip']
                packet['TCP'].sport = self.old_server['port']

                packet = checksum(packet)
                self.filewriter(self.mod_traf_fname, packet)

        # Запрет передачи сообщений
        elif packet.haslayer('UDP') and packet.haslayer('IP'):
            if (packet['IP'].src == self.first_node['ip']
                    and packet['UDP'].sport == self.first_node['port']
                    and packet['IP'].dst == self.second_node['ip']
                    and packet['UDP'].dport == self.second_node['port']) \
                or (packet['IP'].src == self.second_node['ip']
                    and packet['UDP'].sport == self.second_node['port']
                    and packet['IP'].dst == self.first_node['ip']
                    and packet['UDP'].dport == self.first_node['port']):

                # запись пакета в файл с удаленным трафиком
                self.filewriter(self.drop_traf_fname, packet)
                packet = 0
                return packet

        if packet.haslayer('TCP') and packet.haslayer('IP'):
            # Установка throughtput
            packet['IP'].tos = 0x28
            packet = checksum(packet)
            self.filewriter(self.mod_traf_fname, packet)

        return packet