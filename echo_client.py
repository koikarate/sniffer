import socket
import sys
import argparse
import configparser


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-F', '--filename', action="store_true")
    parser.add_argument('-s', '--server_ip', nargs='?', default='192.168.1.15')
    parser.add_argument('-p', '--server_port', nargs='?', default='9090')
    parser.add_argument('-m', '--message', required=True, nargs='+')
    parser.add_argument('-c', '--client_port', nargs='?', default='8181')

    return parser


if __name__ == '__main__':
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])

    # Если был добавлен параметр с конфигурационным файлом, то сразу все настройки берутся из него
    if namespace.filename:
        config = configparser.ConfigParser()
        config.read("client_settings.ini")
        HOST = config["Client"]["server_ip"]
        PORT = int(config["Client"]["server_port"])
        CLIENT_PORT = int(config["Client"]["client_port"])

    else:
        HOST = namespace.server_ip
        PORT = int(namespace.server_port)
        CLIENT_PORT = int(namespace.client_port)

    data = ' '.join(namespace.message)

    # SOCK_DGRAM is the socket type to use for UDP sockets
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    client.bind(("", CLIENT_PORT))

    client.sendto(bytes(data + "\n", "utf-8"), (HOST, PORT))
    received = str(client.recv(1024), "utf-8")

    print("Sent:     {}".format(data))
    print("Received: {}".format(received))
