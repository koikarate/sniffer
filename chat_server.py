import socket
import sys
import argparse
import configparser
import threading

clients = []


# Creates parser to parce parameters
def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-F', '--filename', action="store_true")
    parser.add_argument('-p', '--server_port', nargs='?', default='9090')

    return parser


# Sending message to all clients besides client
def broadcast(data, unwanted_client):
    for client in clients:
        if not client == unwanted_client:
            client.send(data)
        else:
            continue


# Receive data from client, broadcast it to other clients, remove client if disconnect
def handle(client, address):
    while True:
        try:
            data = client.recv(1024).decode('ascii')

            if data:
                data = data + f'[[from IP {address[0]}]]'
                data = data.encode('ascii')
                broadcast(data, client)

            else:
                clients.remove(client)
                client.close()
                break

        except:
            clients.remove(client)
            client.close()
            break


# Receiving / Listening Function
def receive():
    while True:
        # Accept Connection
        client, address = server.accept()

        print(f"\n\nNEW CLIENT\n"
              f"CLIENT IP:   {address[0]}\n"
              f"CLIENT PORT: {address[1]}\n")

        # Add client to clients list adn send info message
        clients.append(client)
        client.send(f'Connected to server! IP:{HOST} PORT:{PORT}'.encode('ascii'))

        # start client_thread for client
        client_thread = threading.Thread(target=handle, args=(client, address))
        client_thread.start()


parser = create_parser()
namespace = parser.parse_args(sys.argv[1:])

# If a parameter with a configuration file was added, then all settings are immediately taken from it
if namespace.filename:
    config = configparser.ConfigParser()
    config.read("server_settings.ini")
    PORT = int(config["Server"]["server_port"])

else:
    PORT = int(namespace.server_port)

# The server's IP address in current configuration
HOST = "192.168.1.13"

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((HOST, PORT))
server.listen()
receive()
