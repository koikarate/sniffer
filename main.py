import socket
from scapy.all import *
import datetime

DTP_TYPES = {
    6: 'TCP',
    17: 'UDP'
}

ARP_TYPES = {
    1: 'APR-REQUEST',
    2: 'APR-RESPONSE',
    3: 'RARP-REQUEST',
    4: 'RARP-RESPONSE'
}

def print_layer_info(packet_num, packet, layers):
    time = datetime.datetime.now()
    wrpcap('sniffed_traffic.pcap', packet, append=True)
	
    if 'Ether' in layers:

        # determining the type of nested data transfer protocol and getting its name from the compliance table
        ether_type = packet['Ether'].type

        print(f"\n\nPacket #{packet_num} - {time} [[ Ether ]] " +
              f"\n\tSIZE: {len(packet['Ether'])}\n\tSOURCE: {packet['Ether'].src}" +
              f"\n\tDESTINATION: {packet['Ether'].dst}" +
              f"\n\tNESTED PROTOCOL: {ether_type}")

    # check for nested protocols
    if 'type' in packet['Ether'].fields:
        if packet['Ether'].type == 2048:

            dtp = packet['IP'].proto
            dtp_name = DTP_TYPES[dtp]

            print(f"\t[[ IP ]] " +
                  f"\n\tSIZE: {len(packet['IP'])}\n\tSOURCE: {packet['IP'].src}" +
                  f"\n\tDESTINATION: {packet['IP'].dst}\n\tTTL: {packet['IP'].ttl}" +
                  f"\n\tDATA TRANSFER PROTOCOL: {dtp_name}")

        elif packet['Ether'].type == 2054:

            arp_type = packet['ARP'].op
            arp_type_name = ARP_TYPES[arp_type]

            print(f"\t[[ ARP ]] " +
                  f"\n\tSIZE: {len(packet['ARP'])}\n\tPACKET TYPE: {arp_type_name}")

    if 'UDP' in layers:
        print(f"\n\nPacket #{packet_num} - {time} [[ UDP ]] " +
              f"\n\tSIZE: {packet['UDP'].len}\n\tSOURCE PORT: {packet['UDP'].sport}" +
              f"\n\tDESTINATION PORT: {packet['UDP'].dport}")

def display(packet):
    global packet_count

    required_layers = ['Ether', 'ARP', 'IP', 'UDP']
    packet_layers = []

    # creating list with required levels available for the packet
    for layer in required_layers:
        if packet.haslayer(layer):
            packet_layers.append(layer)

    # increment packet count
    packet_count += 1

    # print packet information with packet count
    print_layer_info(packet_count, packet, packet_layers)

		
if __name__ == '__main__':

    packet_count = 0

    # Ask user to enter interface names
    interface = input("Enter the interface name to sniff on: ")
	
    # Sniff packets and pass them to display function
    sniff(prn=display, iface=interface)
