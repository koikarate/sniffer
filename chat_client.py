import socket
import sys
import argparse
import configparser
import threading


# Creates parser to parce parameters
def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-F', '--filename', action="store_true")
    parser.add_argument('-s', '--server_ip', nargs='?', default='192.168.1.15')
    parser.add_argument('-p', '--server_port', nargs='?', default='9090')
    parser.add_argument('-c', '--client_port', nargs='?', default='8181')

    return parser


# Function to create and send message from user input to server
def write_message():
    while True:
        data = input('')
        client.send(data.encode('ascii'))
        print("Sent:     {}".format(data))


# Function to receive data from server
def receive():
    while True:
        try:
            received = client.recv(1024).decode('ascii')
            if received:
                print("Received: {}".format(received))
        except:
            print("An error occurred!")
            # client.close()
            break


if __name__ == '__main__':
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])

    # If a parameter with a configuration file was added, then all settings are immediately taken from it
    if namespace.filename:
        config = configparser.ConfigParser()
        config.read("client_settings.ini")
        HOST = config["Client"]["server_ip"]
        PORT = int(config["Client"]["server_port"])
        CLIENT_PORT = int(config["Client"]["client_port"])

    else:
        HOST = namespace.server_ip
        PORT = int(namespace.server_port)
        CLIENT_PORT = int(namespace.client_port)

    # Create a socket and connecting (SOCK_STREAM means a TCP socket)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to a specific address and port and connect to destination
    client.bind(("", CLIENT_PORT))
    client.connect((HOST, PORT))

    receive_thread = threading.Thread(target=receive)
    receive_thread.start()

    write_thread = threading.Thread(target=write_message)
    write_thread.start()
