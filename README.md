# Sniffer, base91 algorithm, echo-server, chat, software switch, traffic modifier

|              Filename               | Description                                               |
|:-----------------------------------:|:----------------------------------------------------------|
|             **main.py**             | Sniffer using Scapy                                       |
|            **base91.py**            | Base91 decoding / encoding realization                    |
|         **echo_server.py**          | Echo-server server part                                   |
|         **echo_client.py**          | Echo-server client part                                   |
|         **chat_server.py**          | Chat-server server part                                   |
|         **chat_client.py**          | Chat-server client part                                   |
|       **server_settings.ini**       | Echo-server server part configuration file                |
|       **client_settings.ini**       | Echo-server client part configuration file                |
|  **SWITCHWORKINGVERSION.py.py**     | Software switch                                           |
| **element_of_commutation_table.py** | Class that implements element of switch commutation table |
|       **modifier.py**               | Traffic sniffer & modifier using Scapy                    |
|  **traffic_modifier_settings.ini**  | Traffic sniffer & modifier configuration file             |