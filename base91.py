import struct
from prettytable import PrettyTable

R91 = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
       'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
       'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
       'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
       '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '#', '$',
       '%', '&', '(', ')', '*', '+', ',', '.', '/', ':', ';', '<', '=',
       '>', '?', '@', '[', ']', '^', '_', '`', '{', '|', '}', '~', '"']


def encode(input_string) -> str:
    # получение строки в виде bytes
    input_string_bytes = bytes(input_string, 'utf-8')
    result = ''
    bits = 0

    # прочитанное количество бит
    read_bits_cnt = 0

    for byte in input_string_bytes:

        # сдвиг байта согласно количеству уже прочитанных бит (добавление, если необходимо, 8 нулей справа)
        # и составление последовательности бит вида [byte1byte2]
        byte = byte << read_bits_cnt
        bits |= byte

        read_bits_cnt += 8

        if read_bits_cnt > 13:

            # получение первых 13 бит из считанной последовательности бит
            value = bits & 0b1111111111111

            # если полученное число <= 88 то считывается один дополнительный 14-й бит,
            # максимально возможное число здесь = 8192 + 88 = 8280 (2^13 + 88) (если в добавленном
            # слева бите оказалась единица, кот. соответствует 2^13)
            # При этом 8280 // 91 == 8280 % 91 == 90 - последний максимально возможный вариант пары
            # двух символов из R91: (R91[90] , R91[90])
            if value <= 88:

                # получение первых 14 бит из считанной последовательности бит
                value = bits & 0b11111111111111

                # удаление прочитанных битов и изменение "остатка"
                bits >>= 14
                read_bits_cnt -= 14

            else:

                # удаление прочитанных битов и изменение "остатка"
                bits >>= 13
                read_bits_cnt -= 13

            # добавление в итоговую строку двух ASCII символа из R91 согласно value
            result += R91[value % 91] + R91[value // 91]

    # если остались еще непрочитанные биты, добавление в конец результата символов
    if not read_bits_cnt == 0:
        result += R91[bits % 91]

    if (read_bits_cnt >= 8) or (bits > 90):
        result += R91[bits // 91]

    return result


def decode(input_string: str) -> bytearray:

    # создание 2-base91-буквенных подстрок input_string
    substrings_arr = [input_string[i:i + 2] for i in range(0, len(input_string), 2)]

    result = bytearray()
    wrote_bits = 0
    origin_value_bits = 0

    for substring in substrings_arr:

        # если последняя подстрока из 1 символа
        if 1 <= len(substring) < 2:
            origin_value = R91.index(substring[0])
            origin_value <<= wrote_bits
            origin_value_bits |= origin_value
            result += struct.pack('B', origin_value_bits & 0b1111111)
            continue

        #TDDO Грамотно обработать исключение
        if not substring[0] in R91 or not substring[1] in R91:
            print("ERROR!")

        else:
            quotient = R91.index(substring[1])
            remainder = R91.index(substring[0])

            origin_value = quotient * 91 + remainder

            # дописывание вновь полученного origin value в старое origin_value_bits,
            # которое может быть еще не до конца записанным
            origin_value <<= wrote_bits
            origin_value_bits |= origin_value

            # увеличение количества записанных бит в зависимости от значения origin_value
            if origin_value_bits & 0b1111111111111 > 88:
                wrote_bits += 13
            else:
                wrote_bits += 14

            # добавление к результату байтов
            while wrote_bits > 7:
                result += struct.pack('B', origin_value_bits & 0b1111111)

                origin_value_bits = origin_value_bits >> 8
                wrote_bits -= 8

    return result


if __name__ == "__main__":
    string1 = 'Hello World'
    string2 = 'Hello World3'

    encoding_table = PrettyTable(['STRING', 'ENCODED STRING'])
    encoding_table.add_row([string1, encode(string1)])
    encoding_table.add_row([string2, encode(string2)])
    print(encoding_table)

    decoding_table = PrettyTable(['STRING', 'DECODED STRING'])
    decoding_table.add_row([">OwJh>Io0Tv!lE", decode(">OwJh>Io0Tv!lE")])
    decoding_table.add_row([">OwJh>Io0Tv!PmG", decode(">OwJh>Io0Tv!PmG")])
    print(decoding_table)

    str1 = 'Hello World This is my base91 algorithm'
    print(decode(encode(str1)))
