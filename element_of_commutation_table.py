import time


class ElementOfCommutationTable (object):
    """
    Represents an element in a commutation table, containing the MAC address and the time it was written.
    """
    def __init__(self, src_mac, input_time):
        """
        Initializes a new instance of the ElementOfCommutationTable class.

        Args:
        src_mac: The MAC address of the element.
        input_time: The time the element was written.
        """
        self.mac = src_mac
        self.write_time = input_time

    def __str__(self):
        """
        Returns a string representation of the element, including the MAC address and the time it was written.

        Returns:
            A string representation of the element.
        """
        return "(" + str(self.mac) + ", " \
                + str(time.localtime(self.write_time).tm_hour) + ":" \
                + str(time.localtime(self.write_time).tm_min) + ":" \
                + str(time.localtime(self.write_time).tm_sec) + ")"

    def __del__(self):
        del self.write_time
        del self.mac
        del self
