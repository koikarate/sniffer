from socketserver import BaseRequestHandler
from socketserver import UDPServer
import sys
import argparse
import configparser


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-F', '--filename', action="store_true")
    parser.add_argument('-p', '--server_port', nargs='?', default='9090')

    return parser


class UDPHandler(BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip()
        socket = self.request[1]

        print(f"\n\nMESSAGE ACCEPTED\n"
              f"CLIENT IP:   {self.client_address[0]}\n"
              f"CLIENT PORT: {self.client_address[1]}\n"
              f"DATA:        {data}")

        try:
            socket.sendto(data, self.client_address)
        except ConnectionError:
            print(f"Client suddenly closed, cannot send")


if __name__ == "__main__":
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])

    # Если был добавлен параметр с конфигурационным файлом, то сразу все настройки берутся из него
    if namespace.filename:
        config = configparser.ConfigParser()
        config.read("server_settings.ini")
        PORT = int(config["Server"]["server_port"])

    else:
        PORT = int(namespace.server_port)

    # The server's IP address
    HOST = "192.168.1.15"

    with UDPServer((HOST, PORT), UDPHandler) as server:
        server.serve_forever()
