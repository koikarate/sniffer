import scapy.all
import os
import time
import argparse
import modifier
import element_of_commutation_table
from threading import Thread
from threading import Lock
from prettytable import PrettyTable


class Switch (object):
    """
    A class that implements a software switch.

    Attributes:
        interfaces (list): A list of strings representing the network interfaces available.
        commutation_table (dict): A dictionary representing the switch table. Each key is a string representing a
                                  network interface, and each value is a list of ElementOfTable objects.
        interface_stats (dict): A dictionary that stores the number of packets processed for each network interface.
        lifetime (int): The maximum age of an entry in the switch table, in seconds.
        packet_lifetime (int): The maximum time a packet has been stored in the switch table, in nanoseconds.
        total_time (int): The total time spent processing packets, in nanoseconds.
        packet_modifier (Modifier): An instance of the Modifier class that can modify packets.
        is_modification_enabled (bool): A flag indicating whether packet modification is enabled.
        locker (Lock): A lock object to synchronize access to the switch table.
    """
    def __init__(self):
        """
        Initializes a new Switch object.
        """
        self.interfaces = [iface for iface in os.listdir("/sys/class/net") if iface != "lo"]
        self.commutation_table = {iface: [] for iface in self.interfaces}
        self.interface_stats = {iface: 0 for iface in self.interfaces}

        self.lifetime = 0
        self.packet_lifetime = 0
        self.total_time = 0
        self.packet_modifier = None
        self.is_modification_enabled = False
        self.locker = Lock()

    def print_commutation_table(self):
        """
        Returns a string representation of the switch table.

        Returns:
            A string representation of the switch table.
        """
        commutation_table = PrettyTable(['Interface', 'Clients'])
        for iface, elements in self.commutation_table.items():
            commutation_table.add_row([iface, {', '.join([str(elem) for elem in elements])}])
        print(commutation_table)

    def handle_packet(self, interface_name, packet, input_time):
        """
        Handles an incoming packet.

        If the packet source is already in the table, updates the last seen timestamp and interface for the source.
        Otherwise, adds a new entry for the source to the table.

        Args:
            interface_name (str): The name of the interface the packet was received on.
            packet (dict): A dictionary representation of the packet.
            input_time (float): The timestamp when the packet was received.
        """
        packet_src = packet['Ether'].src
        packet_dst = packet['Ether'].dst

        if self.is_packet_src_in_table(packet_src):
            self.update_packet_timestamp(interface_name, packet, input_time)
        else:
            self.add_packet_to_table(interface_name, packet, input_time)

        src_iface = self.search_interface_by_mac(packet_src)
        dst_iface = self.search_interface_by_mac(packet_dst)

        if src_iface != interface_name:
            return

        try:
            packet = self.packet_modifier.modify(packet) if self.is_modification_enabled else packet
            if not packet:
                return

            self.interface_stats[interface_name] += 1

            if not dst_iface:
                self.broadcast(packet, interface_name)
            elif src_iface != dst_iface:
                scapy.all.sendp(packet, dst_iface)

            self.calculate_processing_time(input_time)

        except OSError as err:
            print(f"Error occurred: {err}")

    def is_packet_src_in_table(self, packet_src):
        """
        Returns True if the packet source is already in the table, False otherwise.

        Args:
            packet_src (str): The MAC address of the packet source.

        Returns:
            bool: True if the packet source is in the table, False otherwise.
        """
        for iface in self.interfaces:
            if self.is_packet_src_on_iface(iface, packet_src):
                return True
        return False

    def add_packet_to_table(self, src_iface, packet, write_time):
        """
        Adds a new entry to the switch table.

        Args:
            src_iface (str): The name of the interface the packet was received on.
            packet (scapy.layers): The incoming packet to add to the switch table.
            write_time (float): The time at which the packet was received.
        """
        self.locker.acquire()
        commutation_table_element = self.create_element_of_table(packet['Ether'].src, write_time)
        self.commutation_table[src_iface].append(commutation_table_element)
        self.locker.release()

    @staticmethod
    def create_element_of_table(src_mac, receive_time):
        """
        Creates an element of commutation table.

        Args:
          src_mac (str): packet source mac address.
          receive_time (float): The time at which the packet was received.
        """
        element_of_table = element_of_commutation_table.ElementOfCommutationTable(src_mac, receive_time)
        return element_of_table

    def calculate_processing_time(self, write_time):
        """
        Updates the total time spent processing packets.

        Args:
          write_time (float): The time at which the packet was received.
        """
        packet_time_ns = (time.time() - write_time) * 1e06
        self.total_time += packet_time_ns

        if packet_time_ns > self.packet_lifetime:
            self.packet_lifetime = packet_time_ns

    def update_packet_timestamp(self, src_iface, packet, write_time):
        """
        Updates the timestamp of existing entry in the switch table.

        Args:
           src_iface (str): The name of the interface the packet was received on.
           packet (scapy.layers.l2.Ether): The incoming packet to update in the switch table.
           write_time (float): The time at which the packet was received.
        """
        self.locker.acquire()
        self.search_mac_iface(src_iface, packet['Ether'].src).write_time = write_time
        self.locker.release()

    def check_times(self):
        """
        Removes expired entries from the switch table.
        """
        self.locker.acquire()

        for iface in self.commutation_table:
            for elem in self.commutation_table[iface]:
                if time.time() - elem.write_time > self.lifetime:
                    self.commutation_table[iface].remove(elem)

        self.locker.release()

    def broadcast(self, packet, packet_iface):
        """
        Sends a packet to all interfaces except the one it was received on.

        Args:
          packet (scapy.layers.l2.Ether): The packet to broadcast.
          packet_iface (str): The name of the interface the packet was received on.
        """
        for destin in self.interfaces:
            if destin is not packet_iface:
                scapy.all.sendp(packet, iface=destin)

    def is_packet_src_on_iface(self, iface, mac):
        """
        Check if a MAC address is already in the table for a given interface.

        Args:
            iface (str): the name of the interface to check
            mac (str): the MAC address to search for

        Returns:
            bool: True if the MAC address is in the table, False otherwise
        """
        for address in self.commutation_table[iface]:
            if address.mac == mac:
                return True
        return False

    def search_interface_by_mac(self, mac):
        """
        Find the interface name corresponding to a given MAC address.

        Args:
           mac (str): the MAC address to search for

        Returns:
           str or int: the name of the interface corresponding to the MAC address, or 0 if the MAC address is not in the
                       table
        """
        for iface in self.interfaces:
            address = self.search_mac_iface(iface, mac)
            if address != 0:
                return iface
        return 0

    def search_mac_iface(self, iface, mac):
        """
        Search for a MAC address in the table for a given interface.

        Args:
           iface (str): the name of the interface to search
           mac (str): the MAC address to search for

        Returns:
           ElementOfTable or int: the ElementOfTable object representing the MAC address and the time it was added to
                                  the table, or 0 if the MAC address is not in the table for the given interface
        """
        for address in self.commutation_table[iface]:
            if address.mac == mac:
                return address
        return 0

    def count_all_packets(self):
        """
        Count the total number of packets received on all interfaces.

        Returns:
            int: the total number of packets received
        """

        num_of_packets = 0
        self.locker.acquire()

        for iface in self.interface_stats:
            num_of_packets += self.interface_stats[iface]
        self.locker.release()
        return num_of_packets

    def print_statistics(self):
        """
        Print statistics about packet processing.
        """
        num_of_packets = self.count_all_packets()
        self.locker.acquire()

        if num_of_packets == 0:
            medium_proc_time = '0'
        else:
            medium_proc_time = str(int(self.total_time / num_of_packets))

        statistic_table = PrettyTable()
        statistic_table.field_names = ["Number of processed packets", "MAX processing time, ns", "AVG processing time, ns"]
        statistic_table.add_row([str(self.interface_stats), str(int(self.packet_lifetime)), medium_proc_time])
        print(statistic_table)

        self.locker.release()

    def input_modific_data(self):
        """
        Requests data for traffic modification.
        The method asks the user for the maximum payload size for UDP packets,
        the filename to save the modified traffic and the filename to save the dropped traffic.
        """
        try:
            mod_traf_name = str(input("Filename for modified traffic: "))
            drop_traf_name = str(input("Filename for dropped traffic: "))
            self.packet_modifier = modifier.Modifier(mod_traf_name, drop_traf_name)

        except ValueError as err:
            print("Error of input values")
            exit(1)

        self.is_modification_enabled = True


class OnePortSniffer(object):
    def __init__(self, iface, comm_table):
        self.iface = iface
        self.comm_table = comm_table

    def process_packet_on_port(self, packet):
        self.comm_table.handle_packet(self.iface, packet, time.time())

    def sniff_on_port(self, interface):
        scapy.all.sniff(iface=interface, store=False, prn=self.process_packet_on_port)


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--life_time', type=int, help='Lifetime of packet in commutation_table')
    parser.add_argument('-modify', '--modification', action="store_true", help='Enable traffic modification')

    return parser


def main():
    parser = create_parser()
    namespace = parser.parse_args()
    comm_table = Switch()
    comm_table.lifetime = namespace.life_time

    if namespace.modification:
        comm_table.input_modific_data()

    for iface in comm_table.interfaces:
        port = OnePortSniffer(iface, comm_table)
        thread = Thread(target=port.sniff_on_port, args=(iface,))
        thread.start()

    while True:
        try:
            os.system("clear")
            comm_table.print_commutation_table()
            comm_table.print_statistics()
            comm_table.check_times()
            time.sleep(3)
            menu = int(input("\nI - commutation table\n2 - statistic\n3 - stop\n"))

            if menu == 1:
                comm_table.print_commutation_table()

            if menu == 2:
                comm_table.print_statistics()

            if menu == 3:
                comm_table.print_statistics()
                print("Finished")
                exit(1)

        except KeyboardInterrupt as err:
            print("InFinished")
            exit(0)


if __name__ == '__main__':
    main()
